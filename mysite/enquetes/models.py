import datetime
from django.conf import settings
from django.db import models
from django.utils import timezone

###### Modelos relativos a extensão da aplicação exemplo
########################################################

class Rotulo(models.Model):
    titulo = models.CharField(max_length = 25)
    class Meta:
        verbose_name = "Rótulo"
    def __str__(self):
        return self.titulo

class Perfil(models.Model):
    descricao = models.CharField(max_length = 100)
    cidade = models.CharField(max_length = 25)
    pais = models.CharField(max_length = 20)
    generos = models.TextField()
    class Meta:
        verbose_name_plural = "Perfis"
    def __str__(self):
        return self.descricao

class Autor(models.Model):
    nome = models.CharField(max_length = 30)
    idade = models.IntegerField()
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete = models.SET_NULL,
        null = True
    )
    perfil = models.OneToOneField(Perfil, on_delete=models.CASCADE)
    class Meta:
        verbose_name_plural = "Autores"
    def __str__(self):
        return self.nome

##### Modelos originais da aplicação
####################################

class Pergunta(models.Model):
    texto = models.CharField(max_length = 200)
    data_publicacao = models.DateTimeField('Data de publicação')
    data_encerramento = models.DateField('Data de encerramento', null = True)
    rotulos = models.ManyToManyField(Rotulo)
    autor = models.ForeignKey(Autor, on_delete=models.CASCADE, null = True)
    def __str__(self):
        return self.texto
    def resultado(self):
        return self.opcao_set.order_by('-votos')
    def is_encerrada(self):
        return timezone.now().date() > self.data_encerramento
    def publicada_recentemente(self):
        agora = timezone.now()
        return agora-datetime.timedelta(days=1)<=self.data_publicacao<=agora
    publicada_recentemente.admin_order_field = 'data_publicacao'
    publicada_recentemente.boolean = True
    publicada_recentemente.short_description = 'Recente?'

class Opcao(models.Model):
    texto = models.CharField(max_length = 100)
    votos = models.IntegerField(default = 0)
    pergunta = models.ForeignKey(Pergunta, on_delete=models.CASCADE)
    class Meta:
        verbose_name = "Opção"
        verbose_name_plural = "Opções"
    def __str__(self):
        return self.texto


