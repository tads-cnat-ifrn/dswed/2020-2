from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, Http404, JsonResponse
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from .models import Pergunta, Opcao, Rotulo, Perfil, Autor
from .forms import PerguntaForm, OpcaoFormSet
from django.contrib.auth.models import User
from django.contrib import messages

### IndexView - Tela inicial listando TODAS as enquetes
#######################################################
class IndexView(generic.View):
    def get(self, request, *args, **kwargs):
        agora = timezone.now()
        ultimas_enquetes = Pergunta.objects.filter(
            # Remove as enquetes com data de encerramento no passado
            data_encerramento__gt = agora
        ).filter(
            # Remove as enquetes com data de publicação no futuro
            data_publicacao__lte = agora
        ).order_by('-data_publicacao')
        contexto = { 'ultimas_enquetes': ultimas_enquetes }
        return render(request, 'enquetes/index.html', contexto)

### APIIndexView - Retorna a lista de enquetes como JSON
########################################################
class APIIndexView(generic.View):
    def get(self, request, *args, **kwargs):
        agora = timezone.now()
        ultimas_enquetes = Pergunta.objects.filter(
            # Remove as enquetes com data de encerramento no passado
            data_encerramento__gt = agora
        ).filter(
            # Remove as enquetes com data de publicação no futuro
            data_publicacao__lte = agora
        ).order_by('-data_publicacao')
        return JsonResponse(list(ultimas_enquetes.values()), safe=False)

### PorRotuloView - lista as enquetes de um dado rótulo
#######################################################
class PorRotuloView(generic.View):
    def get(self, request, *args, **kwargs):
        rotulo_id = self.kwargs['pk']
        rotulo = get_object_or_404(Rotulo, pk=rotulo_id)
        enquetes = rotulo.pergunta_set.filter(
            data_publicacao__lte = timezone.now()
        ).order_by('-data_publicacao')
        contexto = { 'enquetes': enquetes, 'rotulo': rotulo }
        return render(request, 'enquetes/por_rotulo.html', contexto)

### EncerradasView - lista as enquetes encerradas
#################################################
class EncerradasView(generic.View):
    def get(self, request, *args, **kwargs):
        agora = timezone.now()
        encerradas = Pergunta.objects.filter(
            # Seleciona apenas as enquetes encerradas
            data_encerramento__lt = agora
        ).order_by('-data_publicacao')
        contexto = { 'encerradas': encerradas }
        return render(request, 'enquetes/encerradas.html', contexto)

### DetalhesView - Exibição dos detalhes de uma dada enquete
############################################################
class DetalhesView(generic.View):
    def get(self, request, *args, **kwargs):
        enquete_id = self.kwargs['pk']
        enquete = get_object_or_404(Pergunta, pk=enquete_id)
        agora = timezone.now()
        # Não serão exibidos os detalhes de enquetes com publicação no futuro,
        # bem como aquelas com data de encerramento no passado
        if enquete.data_publicacao > agora or \
           enquete.data_encerramento < agora.date():
            raise Http404('Não existe enquete ativa com essa identificação!')
        return render(request, 'enquetes/detalhes.html', {'pergunta':enquete})

### ResultadoView - Exibição do resultado 'parcial' de uma dada enquete
#######################################################################
class ResultadoView(generic.View):
    def get(self, request, *args, **kwargs):
        enquete_id = self.kwargs['pk']
        enquete = get_object_or_404(Pergunta, pk=enquete_id)
        agora = timezone.now()
        # Não serão exibidos os detalhes de enquetes com publicação no futuro
        if enquete.data_publicacao > agora:
            raise Http404('Não existe enquete ativa com essa identificação!')
        # Para enquetes já encerradas, deve ser apresentado o resultado final
        elif enquete.data_encerramento < agora.date():
            total = 0
            for opcao in enquete.opcao_set.all():
                total += opcao.votos
            return render(
                request, 'enquetes/resultado_final.html',
                {'enquete': enquete, 'total': total}
            )
        return render(request, 'enquetes/resultados.html', {'enquete':enquete})

### VocacaoView - Registro de voto em uma das opções de uma dada enquete
########################################################################
class VotacaoView(generic.View):
    def post(self, request, *args, **kwargs):
        enquete_id = self.kwargs['enquete_id']
        enquete = get_object_or_404(Pergunta, pk=enquete_id)
        try:
            op_desejada = enquete.opcao_set.get(pk=request.POST['opcao'])
        except (KeyError, Opcao.DoesNotExist):
            contexto = {
                'pergunta': enquete,
                'error_message': "ERRO: Deve ser selecionada uma opção válida!",
            }
            return render(request, 'enquetes/detalhes.html', contexto)
        else:
            op_desejada.votos += 1
            op_desejada.save()
            return HttpResponseRedirect(reverse('enquetes:resultado',
                args=(enquete.id,)))

### FormEnqueteView - Cadastramento de enquete com classes de form
##################################################################
@method_decorator(login_required, 'dispatch')
class FormEnqueteView(generic.View):
    def get(self, request, *args, **kwargs):
        form_pergunta = PerguntaForm()
        formset_opcao = OpcaoFormSet(queryset=Opcao.objects.none())
        contexto = {
            'form_pergunta': form_pergunta, 'formset_opcao': formset_opcao
        }
        return render(request, 'enquetes/form_enquete.html', contexto)
    def post(self, request, *args, **kwargs):
        form_pergunta = PerguntaForm(request.POST)
        formset_opcao = OpcaoFormSet(request.POST)
        if form_pergunta.is_valid() and formset_opcao.is_valid():
            pergunta = form_pergunta.save(commit=False)
            pergunta.data_publicacao = timezone.now()
            pergunta.autor = request.user.autor
            pergunta.save()
            form_pergunta.save_m2m()
            opcoes = formset_opcao.save(commit=False)
            for opcao in opcoes:
                opcao.pergunta = pergunta
                opcao.save()
        else:
            contexto = {
                'form_pergunta': form_pergunta, 'formset_opcao': formset_opcao
            }
            return render(request, 'enquetes/form_enquete.html', contexto)
        messages.success(request, 'Enquete cadastrada com sucesso!')
        return HttpResponseRedirect(reverse('enquetes:index'))

### EditarEnqueteView - Possibilita a edição dos detalhes de uma enquete
########################################################################
@method_decorator(login_required, 'dispatch')
class EditarEnqueteView(generic.View):
    def get(self, request, *args, **kwargs):
        pk = self.kwargs['pk']
        enquete = get_object_or_404(Pergunta, pk=pk)
        if enquete.autor.user != self.request.user:
            messages.warning(
                request, 'Uma enquete só pode ser editada pelo seu autor!'
            )
            return HttpResponseRedirect(reverse('enquetes:index'))
        if enquete.is_encerrada():
            messages.warning(
                request, 'Não é possível editar enquetes encerradas!'
            )
            return HttpResponseRedirect(reverse('enquetes:index'))
        form_pergunta = PerguntaForm(instance=enquete)
        formset_opcao = OpcaoFormSet(
            queryset=Opcao.objects.filter(pergunta=enquete)
        )
        contexto = {
            'id': pk,
            'form_pergunta': form_pergunta,
            'formset_opcao': formset_opcao
        }
        return render(request, 'enquetes/atualiza_enquete.html', contexto)
    def post(self, request, *args, **kwargs):
        pk = self.kwargs['pk']
        enquete = get_object_or_404(Pergunta, pk=pk)
        if enquete.autor.user != self.request.user:
            messages.warning(
                request, 'Uma enquete só pode ser editada pelo seu autor!'
            )
            return HttpResponseRedirect(reverse('enquetes:index'))
        form_pergunta = PerguntaForm(request.POST, instance=enquete)
        formset_opcao = OpcaoFormSet(request.POST)
        if form_pergunta.is_valid() and formset_opcao.is_valid():
            pergunta = form_pergunta.save()
            opcoes = formset_opcao.save(commit=False)
            for op in formset_opcao.deleted_objects:
                op.delete()
            for opcao in opcoes:
                opcao.pergunta = pergunta
                opcao.save()
        else:
            contexto = {
                'id': pk,
                'form_pergunta': form_pergunta,
                'formset_opcao': formset_opcao
            }
            return render(request, 'enquetes/atualiza_enquete.html', contexto)
        messages.success(request, 'Enquete atualizada com sucesso!')
        return HttpResponseRedirect(reverse('enquetes:index'))

### NovaEnqueteView - Possibilita o cadastramento de uma nova enquete
#####################################################################
@method_decorator(login_required, 'dispatch')
class NovaEnqueteView(generic.View):
    def get(self, request, *args, **kwargs):
        rotulos = Rotulo.objects.all()
        return render(
            request, 'enquetes/nova_enquete.html', {'rotulos': rotulos}
        )
    def post(self, request, *args, **kwargs):
        enunciado = request.POST.get('enunciado', '')
        encerramento = request.POST.get('encerramento', '')
        op1 = request.POST.get('op1', '')
        op2 = request.POST.get('op2', '')
        op3 = request.POST.get('op3', '')
        op4 = request.POST.get('op4', '')
        op5 = request.POST.get('op5', '')
        op6 = request.POST.get('op6', '')
        rotulos = request.POST.getlist('rotulo', [])
        if enunciado and encerramento and op1 and op2 and rotulos:
            enquete = Pergunta.objects.create(
                texto=enunciado, data_publicacao=timezone.now(),
                data_encerramento=encerramento, autor=request.user.autor
            )
            for rotulo_id in rotulos:
                r = Rotulo.objects.get(pk=rotulo_id)
                enquete.rotulos.add(r)
            if op1:
                op = Opcao(texto=op1)
                op.pergunta = enquete
                op.save()
            if op2:
                op = Opcao(texto=op2)
                op.pergunta = enquete
                op.save()
            if op3:
                op = Opcao(texto=op3)
                op.pergunta = enquete
                op.save()
            if op4:
                op = Opcao(texto=op4)
                op.pergunta = enquete
                op.save()
            if op5:
                op = Opcao(texto=op5)
                op.pergunta = enquete
                op.save()
            if op6:
                op = Opcao(texto=op6)
                op.pergunta = enquete
                op.save()
        else:
            messages.error(request, 'Obrigatório o preenchimento do enunciado,'+\
            ' data encerramento, ao menos um rótulo e das duas primeiras opções!')
            rotulos = Rotulo.objects.all()
            return render(
                request, 'enquetes/nova_enquete.html', {'rotulos': rotulos}
            )
        messages.success(request, 'Enquete cadastrada com sucesso!')
        return HttpResponseRedirect(reverse('enquetes:index'))

### NovoUsuarioView - Possibilita o cadastramento de novos usuários
###################################################################
class NovoUsuarioView(generic.View):
    def get(self, request, *args, **kwargs):
        return render(request, 'enquetes/novo_usuario.html')
    def post(self, request, *args, **kwargs):
        # Recuperando os parâmetros enviados no formulário
        login = request.POST.get('login', '')
        email = request.POST.get('email', '')
        senha1 = request.POST.get('senha1', '')
        senha2 = request.POST.get('senha2', '')
        firstname = request.POST.get('firstname', '')
        lastname = request.POST.get('lastname', '')
        idade = request.POST.get('idade', '')
        descricao = request.POST.get('descricao', '')
        cidade = request.POST.get('cidade', '')
        pais = request.POST.get('pais', '')
        generos = request.POST.get('generos', '')
        # Verificando se todos os parâmetros foram informados
        if login and senha1 and senha2 and firstname and lastname and \
           idade and descricao and cidade and pais and generos:
            # Verificando se as senhas informadas são diferentes
            if senha1 != senha2:
                messages.error(request, 'ERRO: Senhas informadas são diferentes!')
                return render(request, 'enquetes/novo_usuario.html')
            else:
                # Criando os registros correspodentes
                user = User.objects.create_user(login, email, senha1)
                user.first_name = firstname
                user.last_name = lastname
                user.save()
                perfil = Perfil.objects.create(
                    descricao=descricao,
                    cidade=cidade,
                    pais=pais,
                    generos=generos
                )
                autor = Autor.objects.create(
                    nome = firstname + ' ' + lastname,
                    idade=idade,
                    user=user,
                    perfil=perfil
                )
                messages.success(request, 'Usuário cadastrado com sucesso!')
                return HttpResponseRedirect(reverse('login')+'?next=/enquetes')
        else:
            messages.error(request, 'ERRO: Todos os campos precisam ser informados!')
            return render(request, 'enquetes/novo_usuario.html')


### PerfilAutorView - Apresenta o perfil de um autor específico
###############################################################
class PerfilAutorView(generic.View):
    def get(self, request, *args, **kwargs):
        id_autor = self.kwargs['pk']
        autor = get_object_or_404(Autor, pk=id_autor)
        return render(request, 'enquetes/perfil_autor.html', {'autor': autor})

"""
############## Outras Opções para os Elementos de View ##############
#####################################################################

####### VIEW INDEX
##################

### OPÇÃO 1 - função de view

def index(request):
    ultimas_enquetes = Pergunta.objects.order_by('-data_publicacao')[:5]
    contexto = { 'ultimas_enquetes': ultimas_enquetes }
    return render(request, 'enquetes/index.html', contexto)

### OPÇÃO 2 - herdando da classe genérica ListView

class IndexView(generic.ListView):
    template_name = 'enquetes/index.html'
    context_object_name = 'ultimas_enquetes'
    def get_queryset(self):
        return Pergunta.objects.order_by('-data_publicacao')[:5]

####### VIEW DETALHES
#####################

### OPÇÃO 1 - função de view

def detalhes(request, enquete_id):
    enquete = get_object_or_404(Pergunta, pk=enquete_id)
    return render(request, 'enquetes/detalhes.html', {'enquete':enquete})

### OPÇÃO 2 - herdando da classe genérica DetailView

class DetalhesView(generic.DetailView):
    model = Pergunta

####### VIEW RESULTADO
######################

### OPÇÃO 1 - função de view

def resultados(request, enquete_id):
    enquete = get_object_or_404(Pergunta, pk=enquete_id)
    return render(request, 'enquetes/resultados.html', {'enquete':enquete})

### OPÇÃO 2 - herdando da classe genérica DetailView

class ResultadoView(generic.DetailView):
    model = Pergunta
    context_object_name = 'enquete'
    template_name = 'enquetes/resultados.html'

####### VIEW VOTAÇÃO
####################

### OPÇÃO 1 - função de view

def votacao(request, enquete_id):
    enquete = get_object_or_404(Pergunta, pk=enquete_id)
    try:
        op_desejada = enquete.opcao_set.get(pk=request.POST['opcao'])
    except (KeyError, Opcao.DoesNotExist):
        contexto = {
            'enquete': enquete,
            'error_message': "Deve ser selecionada uma opção válida!",
        }
        return render(request, 'enquetes/detalhes.html', contexto)
    else:
        op_desejada.votos += 1
        op_desejada.save()
        return HttpResponseRedirect(reverse('enquetes:resultado',
            args=(enquete.id,)))

"""