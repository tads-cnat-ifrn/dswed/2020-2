from .models import Pergunta, Opcao
from django.forms import ModelForm, modelformset_factory, \
CheckboxSelectMultiple, Textarea

class PerguntaForm(ModelForm):
    class Meta:
        model = Pergunta
        fields = ['texto', 'data_encerramento', 'rotulos']
        widgets = {
            'texto': Textarea(attrs={'cols': 50, 'rows': 2}),
            'rotulos': CheckboxSelectMultiple(),
        }
        help_texts = {
            'data_encerramento': ('Ex.: 2006-10-25'),
        }

class OpcaoForm(ModelForm):
    class Meta:
        model = Opcao
        fields = ['texto']

OpcaoFormSet = modelformset_factory(
    Opcao, form=OpcaoForm, extra=6, can_delete=True
)

