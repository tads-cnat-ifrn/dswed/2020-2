from django.urls import path
from . import views

app_name = 'enquetes'
urlpatterns = [
    path(
        '', views.IndexView.as_view(), name='index'
    ),
    path(
        'api/list/', views.APIIndexView.as_view(), name='api_list'
    ),
    path(
        'adicionar/', views.NovaEnqueteView.as_view(), name='nova'
    ),
    path(
        'adicionar2/', views.FormEnqueteView.as_view(), name='nova2'
    ),
    path(
        '<int:pk>/editar/', views.EditarEnqueteView.as_view(), name='editar'
    ),
    path(
        '<int:pk>/', views.DetalhesView.as_view(), name='detalhes'
    ),
    path(
        '<int:pk>/resultado/', views.ResultadoView.as_view(), name='resultado'
    ),
    path(
        'encerradas/', views.EncerradasView.as_view(), name='encerradas'
    ),
    path(
        '<int:enquete_id>/votacao/', views.VotacaoView.as_view(), name='votacao'
    ),
    path(
        'rotulo/<int:pk>/', views.PorRotuloView.as_view(), name='por_rotulo'
    ),
    path(
        'autor/cadastrar/', views.NovoUsuarioView.as_view(), name='novo_usuario'
    ),
    path(
        'autor/<int:pk>/perfil/', views.PerfilAutorView.as_view(), name='perfil'
    ),
]