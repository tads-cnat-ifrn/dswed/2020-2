from django.contrib import admin
from django.db import models
from django.forms import CheckboxSelectMultiple
from .models import Pergunta, Opcao, Rotulo, Autor, Perfil

### Formulário de Perguntas
###########################
class OpcaoInline(admin.TabularInline):
    model = Opcao
    extra = 2

class PerguntaAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple()},
    }
    fieldsets = [
        (None, {'fields': ['texto', 'rotulos']}),
        ('Informações de Autor', {'fields': ['autor']}),
        ('Informações de Data', {
            'fields': ['data_publicacao', 'data_encerramento']
        }),
    ]
    inlines = [OpcaoInline]
    list_display = (
        'texto', 'id', 'autor',
        'data_publicacao', 'data_encerramento',
        'publicada_recentemente'
    )
    search_fields = ['texto']

admin.site.register(Pergunta, PerguntaAdmin)
###########################

### Formulário de Rótulos
#########################
class RotuloAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'id')
    search_fields = ['titulo']

admin.site.register(Rotulo, RotuloAdmin)
#########################

### Formulário de Autores
##########################

class AutorInline(admin.StackedInline):
    model = Autor
    verbose_name_plural = 'Identificação'

class PerfilAdmin(admin.ModelAdmin):
    inlines = [AutorInline]
    fieldsets = [
        ('Informações de Perfil', {
            'fields': ['descricao', 'cidade', 'pais', 'generos']
        }),
    ]
    list_display = ('autor', 'id', 'cidade', 'pais')
    search_fields = ['autor']

admin.site.register(Perfil, PerfilAdmin)
##########################

admin.AdminSite.site_header = 'Aplicação de Enquetes - Administração'
admin.AdminSite.index_title = 'Aplicações e Modelos a Administrar'