# Generated by Django 2.2.7 on 2021-02-12 17:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('enquetes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Perfil',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('decricao', models.CharField(max_length=100)),
                ('cidade', models.CharField(max_length=25)),
                ('pais', models.CharField(max_length=20)),
                ('generos', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Rotulo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=25)),
            ],
        ),
        migrations.CreateModel(
            name='Autor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=30)),
                ('idade', models.IntegerField()),
                ('perfil', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='enquetes.Perfil')),
            ],
        ),
        migrations.AddField(
            model_name='pergunta',
            name='autor',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='enquetes.Autor'),
        ),
        migrations.AddField(
            model_name='pergunta',
            name='rotulos',
            field=models.ManyToManyField(to='enquetes.Rotulo'),
        ),
    ]
