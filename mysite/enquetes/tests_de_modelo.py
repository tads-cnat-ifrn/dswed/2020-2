import datetime
from django.utils import timezone
from django.test import TestCase
from .models import Pergunta

###### Testes da classe Pergunta (modelo)
#########################################
class PerguntaTest(TestCase):
    def test_publicada_recentemente_com_data_no_futuro(self):
        """
        O método publicada_recentemente() deve retornar False para Perguntas
        com data de publicação no futuro.
        """
        data_teste = timezone.now() + datetime.timedelta(days=30)
        pergunta = Pergunta(data_publicacao = data_teste)
        self.assertIs(pergunta.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_no_passado(self):
        """
        O método publicada_recentemente() deve retornar False para Perguntas
        com data de publicação anterior às últimas 24hs.
        """
        data_teste = timezone.now() - datetime.timedelta(days=1,seconds=1)
        pergunta = Pergunta(data_publicacao = data_teste)
        self.assertIs(pergunta.publicada_recentemente(), False)

    def test_publicacda_recentemente_com_data_dentro_das_24hs(self):
        """
        O método publicada_recentemente() deve retornar True para Perguntas
        com data de publicação dentro das últimas 24hs.
        """
        data_teste = timezone.now() - datetime.timedelta(hours=23,minutes=59,seconds=59)
        pergunta = Pergunta(data_publicacao = data_teste)
        self.assertIs(pergunta.publicada_recentemente(), True)

